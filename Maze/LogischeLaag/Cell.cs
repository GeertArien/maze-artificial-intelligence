﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <author>Geert Ariën</author>
/// <date>24 Mei 2014</date>

namespace Doolhof.LogischeLaag {

    /// <summary>
    /// Klasse die de gegevens van een cel bevat
    /// </summary>
    class Cell {

        /// <summary>
        /// Positie in de x-as
        /// </summary>
        private int positionX;

        /// <summary>
        /// Positie in de y-as
        /// </summary>
        private int positionY;

        /// <summary>
        /// Of er al dan niet een muur is aan de bovenkant van de cel
        /// </summary>
        private bool wallTop;

        /// <summary>
        /// Of er al dan niet een muur is aan de onderkant van de cel
        /// </summary>
        private bool wallBottom;

        /// <summary>
        /// Of er al dan niet een muur is aan de linkerkant van de cel
        /// </summary>
        private bool wallLeft;

        /// <summary>
        /// Of er al dan niet een muur is aan de rechterkant van de cel
        /// </summary>
        private bool wallRight;

        /// <summary>
        /// Of de cel nog aanliggende buren heeft die nog niet bezocht zijn
        /// </summary>
        private bool unvisitedNeighbours;

        /// <summary>
        /// De parent cel van deze cel
        /// </summary>
        private Cell parent;

        /// <summary>
        /// De reële kost om van de start cel tot deze cel te geraken
        /// </summary>
        private int gcost;

        /// <summary>
        /// De reële kost om tot deze cel te geraken, plus de geschatte kost om tot de eindcel te geraken
        /// </summary>
        private int fcost;

        /// <summary>
        /// Property voor de x positie
        /// </summary>
        public int PositionX {
            get {
                return this.positionX;
            }
        }

        /// <summary>
        /// Property voor de y positie
        /// </summary>
        public int PositionY {
            get {
                return this.positionY;
            }
        }

        /// <summary>
        /// Property voor de bovenmuur
        /// </summary>
        public bool WallTop {
            set {
                this.wallTop = value;
            }
            get {
                return this.wallTop;
            }
        }

        /// <summary>
        /// Property voor de ondermuur
        /// </summary>
        public bool WallBottom {
            set {
                this.wallBottom = value;
            }
            get {
                return this.wallBottom;
            }
        }

        /// <summary>
        /// Property voor de linkermuur
        /// </summary>
        public bool WallLeft {
            set {
                this.wallLeft = value;
            }
            get {
                return this.wallLeft;
            }
        }

        /// <summary>
        /// Property voor de rechtermuur
        /// </summary>
        public bool WallRight {
            set {
                this.wallRight = value;
            }
            get {
                return this.wallRight;
            }
        }

        /// <summary>
        /// Property voor onbezochte buren
        /// </summary>
        public bool UnvisitedNeighbours {
            set {
                this.unvisitedNeighbours = value;
            }
            get {
                return this.unvisitedNeighbours;
            }
        }

        /// <summary>
        /// Property voor de parent cel
        /// </summary>
        public Cell Parent {
            set {
                this.parent = value;
            }
            get {
                return this.parent;
            }
        }

        /// <summary>
        /// Property voor de reële kost
        /// </summary>
        public int Gcost {
            set {
                this.gcost = value;
            }
            get {
                return this.gcost;
            }
        }

        /// <summary>
        /// Property voor de reële plus de geschatte kost
        /// </summary>
        public int Fcost {
            set {
                this.fcost = value;
            }
            get {
                return this.fcost;
            }
        }

        /// <summary>
        /// Niet-standaard constructor.
        /// </summary>
        /// <param name="positionX">int: de positie in de x-as</param>
        /// <param name="positionY">int: de positie in de y-as</param>
        public Cell(int positionX, int positionY) {
            this.positionX = positionX;
            this.positionY = positionY;
        }

        /// <summary>
        /// Reset de gegevens van de cel, buiten de x en y positie
        /// </summary>
        public void reset() {
            this.WallRight = true;
            this.WallTop = true;
            this.WallLeft = true;
            this.WallBottom = true;
            this.UnvisitedNeighbours = true;
            this.Parent = null;
            this.Fcost = 0;
            this.Gcost = 0;
        }
    }
}
