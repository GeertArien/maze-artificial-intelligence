﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <author>Geert Ariën</author>
/// <date>24 Mei 2014</date>

namespace Doolhof.LogischeLaag {

    /// <summary>
    /// Deze klasse voorziet functionaliteit voor het genereren en oplossen van een doolhof
    /// </summary>
    class Maze {

        /// <summary>
        /// 2D array waarin de cellen van het doolhof verpakt zitten
        /// </summary>
        private Cell[,] cells;

        /// <summary>
        /// De start cel van het doolhof
        /// </summary>
        private Cell startCell;

        /// <summary>
        /// De eind cel van het doolhof
        /// </summary>
        private Cell endCell;

        /// <summary>
        /// Een instantie van de random klasse
        /// </summary>
        Random random;

        /// <summary>
        /// Property voor de 2D cellen array
        /// </summary>
        public Cell[,] Cells {
            get {
                return this.cells;
            }
        }

        /// <summary>
        /// Property voor de start cel
        /// </summary>
        public Cell StartCell {
            set {
                this.startCell = value;
            }
            get {
                return this.startCell;
            }
        }

        /// <summary>
        /// Property voor de eind cel
        /// </summary>
        public Cell EndCell {
            set {
                this.endCell = value;
            }
            get {
                return this.endCell;
            }
        }

        /// <summary>
        /// Niet-standaard constructor
        /// </summary>
        /// <param name="width">int: de breedte van het doolhof</param>
        /// <param name="height">int: de hoogte van het doolhof</param>
        public Maze(int width, int height) {
            this.cells = new Cell[width, height];

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    this.cells[j, i] = new Cell(j, i);
                }
            }
            this.random = new Random();
        }

        /// <summary>
        /// Een doolhof genereren
        /// </summary>
        public void generateMaze() {
            this.resetCells();
            this.GenereteWalls();
        }

        /// <summary>
        /// De standaard waarden van elke cel terug instellen
        /// </summary>
        private void resetCells() {
            foreach (Cell cell in this.cells) {
                cell.reset();
            }
        }

        /// <summary>
        /// De muren van het doolhof genereren
        /// </summary>
        private void GenereteWalls() {

            Stack<Cell> visitedCells = new Stack<Cell>();

            Cell cell = this.cells[0, 0];

            do {
                // Als de cel nog niet bezocht is geweest, zet ze dan in de lijst van bezochte cellen
                if (!visitedCells.Contains(cell)) {
                    visitedCells.Push(cell);
                }

                Cell neighbour = null;

                // Als de cel nog niet bezochte buurcellen heeft, haal er dan willekeurig een van op
                if (cell.UnvisitedNeighbours) {
                    neighbour = this.GetUnvisitedNeighbour(cell, visitedCells);
                }

                // Als er een onbezochte buur is
                if (neighbour != null) {
                    // Verwijder de juiste muur tussen beide cellen
                    if (cell.PositionY == neighbour.PositionY) {
                        if (cell.PositionX < neighbour.PositionX) {
                            cell.WallRight = false;
                            neighbour.WallLeft = false;
                        }
                        else {
                            cell.WallLeft = false;
                            neighbour.WallRight = false;
                        }
                    }
                    else {
                        if (cell.PositionY < neighbour.PositionY) {
                            cell.WallTop = false;
                            neighbour.WallBottom = false;
                        }
                        else {
                            cell.WallBottom = false;
                            neighbour.WallTop = false;
                        }
                    }
                    // Zet deze cel als de parent van de buurcel
                    neighbour.Parent = cell;
                    // Zet de buurcel als volgende cel die moet verwerkt worden
                    cell = neighbour;
                }
                // Er is geen onbezochte buurcel gevonden is
                else {
                    // Zet de parent van deze cel als cel die moet verwerkt worden
                    cell = cell.Parent;
                }
            // Voer deze instructies uit todat al de cellen bezocht zijn.
            } while (visitedCells.Count < this.cells.Length);
        }


        /// <summary>
        /// Van een bepaalde cel een willekeurige naburige cel ophalen die nog niet bezocht is
        /// Geef null terug indien er geen buur is die aan de voorwaarden voldoet
        /// </summary>
        /// <param name="cell">Cell: de cel waarvan een buur wordt gezocht</param>
        /// <param name="visitedCells">Stack: de cellen die reeds bezocht zijn</param>
        /// <returns>Cell: een willekeurige naburige onbezochte cel</returns>
        private Cell GetUnvisitedNeighbour(Cell cell, Stack<Cell> visitedCells) {

            Cell neighbour = null;
            List<Cell> neighbours = new List<Cell>();

            if (cell.PositionX + 1 < this.cells.GetLength(0)) {
                if (!visitedCells.Contains(this.cells[cell.PositionX + 1, cell.PositionY])) {
                    neighbours.Add(this.cells[cell.PositionX + 1, cell.PositionY]);
                }
            }
            if (cell.PositionY + 1 < this.cells.GetLength(1)) {
                if (!visitedCells.Contains(this.cells[cell.PositionX, cell.PositionY + 1])) {
                    neighbours.Add(this.cells[cell.PositionX, cell.PositionY + 1]);
                }
            }
            if (cell.PositionX > 0) {
                if (!visitedCells.Contains(this.cells[cell.PositionX - 1, cell.PositionY])) {
                    neighbours.Add(this.cells[cell.PositionX - 1, cell.PositionY]);
                }
            }
            if (cell.PositionY > 0) {
                if (!visitedCells.Contains(this.cells[cell.PositionX, cell.PositionY - 1])) {
                    neighbours.Add(this.cells[cell.PositionX, cell.PositionY - 1]);
                }
            }

            // Als er maar 1 onbezochte buur is ,zet dan unvisited neighbours op false
            if (neighbours.Count == 1) {
                neighbour = neighbours[0];
                cell.UnvisitedNeighbours = false;
            }
            else if (neighbours.Count > 0) {
                neighbour = neighbours[this.random.Next(0, neighbours.Count)];
            }

            return neighbour;
        }


        /// <summary>
        /// Het beste pat genereren voor de oplossing van het doolhof
        /// </summary>
        public void generatePath() {
            this.resetParents();
            List<Cell> openList = new List<Cell>();
            List<Cell> closedList = new List<Cell>();

            // Zet voor de start cel de Gcost op 0 en bereken de fcost door de hcost hieraan toe te voegen
            this.startCell.Gcost = 0;
            this.startCell.Fcost = Math.Abs(this.startCell.PositionX - this.endCell.PositionX) + Math.Abs(this.startCell.PositionY - this.endCell.PositionY);
            // Voeg de startcel toe aan de open list
            openList.Add(this.startCell);

            do {
                // Selecteer de cel met de kleinste fcost uit de openlist
                Cell cell = openList.First();
                // Verwijder deze cel van de openlist en zet ze in de closed list
                openList.Remove(cell);
                closedList.Add(cell);

                // Haal de bereikbare buren van deze cel op
                List<Cell> neighbours = this.getReachableNeighbours(cell);
                // Voor elke buurcel
                foreach (Cell neighbour in neighbours) {
                    // Als deze buurcel al in de gesloten lijst zit, negeer ze dan
                    if (!closedList.Contains(neighbour)) {
                        // Anders bereken dan de gcost door 1 toe te voegen aan de gcost van de huidige cel
                        int gcost = cell.Gcost + 1;
                        // Als de buurcel nog niet in de openlijst zit
                        if (!openList.Contains(neighbour)) {
                            // Zet de huidige cel als parent van deze cel
                            neighbour.Parent = cell;
                            // Zet de gcost als gcost voor deze cel
                            neighbour.Gcost = gcost;
                            // Bereken de hcost
                            int hcost = Math.Abs(neighbour.PositionX - this.endCell.PositionX) + Math.Abs(neighbour.PositionY - this.endCell.PositionY);
                            // Bereken de fcost door de hcost aan de gcost toe te voegen
                            neighbour.Fcost = neighbour.Gcost + hcost;
                            // Voeg de cel toe aan de open list
                            openList.Add(neighbour);
                        }
                        // Als de buurcel wel in de open list zit maar een grotere gcost heeft
                        else if (gcost < neighbour.Gcost) {
                            // Verander de parent van de buurcel naar de huidige cel
                            neighbour.Parent = cell;
                            // Zet de nieuwe gcost
                            neighbour.Gcost = gcost;
                            // Bereken de hcost
                            int hcost = Math.Abs(neighbour.PositionX - this.endCell.PositionX) + Math.Abs(neighbour.PositionY - this.endCell.PositionY);
                            // Bereken de nieuwe fcost door de hcost aan de nieuwe gcost toe te voegen
                            neighbour.Fcost = neighbour.Gcost + hcost;
                        }
                    }
                }
                // Rangschik de open list op fcost
                openList = openList.OrderBy(x => x.Fcost).ToList<Cell>();   
            // Blijf dit herhalen tot de openlist leeg is of tot de eindcel in de openlist zit
            } while (!openList.Contains(this.endCell) && openList.Count != 0);
        }


        /// <summary>
        /// De naburige cellen ophalen die vanaf een bepaalde cel bereikbaar zijn
        /// </summary>
        /// <param name="cell">Cell: de cel van waar wordt vertrokken</param>
        /// <returns></returns>
        private List<Cell> getReachableNeighbours(Cell cell) {
            List<Cell> neighbours = new List<Cell>();

            if (!cell.WallRight) {
                neighbours.Add(this.cells[cell.PositionX +1, cell.PositionY]);
            }
            if (!cell.WallTop) {
                neighbours.Add(this.cells[cell.PositionX, cell.PositionY + 1]);
            }
            if (!cell.WallLeft) {
                neighbours.Add(this.cells[cell.PositionX - 1, cell.PositionY]);
            }
            if (!cell.WallBottom) {
                neighbours.Add(this.cells[cell.PositionX, cell.PositionY - 1]);
            }

            return neighbours;
        }

        /// <summary>
        /// De parent cel van elke cel resetten
        /// </summary>
        private void resetParents() {
            foreach (Cell cell in this.cells) {
                cell.Parent = null;
            }
        }
    }
}
