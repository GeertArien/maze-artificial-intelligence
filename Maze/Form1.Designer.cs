﻿namespace Doolhof {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.picutreBox = new System.Windows.Forms.PictureBox();
            this.buttonGenerateMaze = new System.Windows.Forms.Button();
            this.buttonGeneratePath = new System.Windows.Forms.Button();
            this.numericUpDownRows = new System.Windows.Forms.NumericUpDown();
            this.labelColumns = new System.Windows.Forms.Label();
            this.labelRows = new System.Windows.Forms.Label();
            this.numericUpDownColumns = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.picutreBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRows)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColumns)).BeginInit();
            this.SuspendLayout();
            // 
            // picutreBox
            // 
            this.picutreBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picutreBox.Location = new System.Drawing.Point(12, 12);
            this.picutreBox.Name = "picutreBox";
            this.picutreBox.Size = new System.Drawing.Size(500, 500);
            this.picutreBox.TabIndex = 0;
            this.picutreBox.TabStop = false;
            // 
            // buttonGenerateMaze
            // 
            this.buttonGenerateMaze.Location = new System.Drawing.Point(547, 107);
            this.buttonGenerateMaze.Name = "buttonGenerateMaze";
            this.buttonGenerateMaze.Size = new System.Drawing.Size(112, 44);
            this.buttonGenerateMaze.TabIndex = 1;
            this.buttonGenerateMaze.Text = "Generate maze";
            this.buttonGenerateMaze.UseVisualStyleBackColor = true;
            this.buttonGenerateMaze.Click += new System.EventHandler(this.buttonGenerateMaze_Click);
            // 
            // buttonGeneratePath
            // 
            this.buttonGeneratePath.Location = new System.Drawing.Point(547, 169);
            this.buttonGeneratePath.Name = "buttonGeneratePath";
            this.buttonGeneratePath.Size = new System.Drawing.Size(115, 44);
            this.buttonGeneratePath.TabIndex = 2;
            this.buttonGeneratePath.Text = "Generate path";
            this.buttonGeneratePath.UseVisualStyleBackColor = true;
            this.buttonGeneratePath.Click += new System.EventHandler(this.buttonGeneratePath_Click);
            // 
            // numericUpDownRows
            // 
            this.numericUpDownRows.Location = new System.Drawing.Point(611, 34);
            this.numericUpDownRows.Maximum = new decimal(new int[] {
            75,
            0,
            0,
            0});
            this.numericUpDownRows.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownRows.Name = "numericUpDownRows";
            this.numericUpDownRows.Size = new System.Drawing.Size(48, 20);
            this.numericUpDownRows.TabIndex = 3;
            this.numericUpDownRows.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // labelColumns
            // 
            this.labelColumns.AutoSize = true;
            this.labelColumns.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelColumns.Location = new System.Drawing.Point(547, 74);
            this.labelColumns.Name = "labelColumns";
            this.labelColumns.Size = new System.Drawing.Size(58, 13);
            this.labelColumns.TabIndex = 4;
            this.labelColumns.Text = "Columns:";
            // 
            // labelRows
            // 
            this.labelRows.AutoSize = true;
            this.labelRows.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRows.Location = new System.Drawing.Point(563, 36);
            this.labelRows.Name = "labelRows";
            this.labelRows.Size = new System.Drawing.Size(42, 13);
            this.labelRows.TabIndex = 5;
            this.labelRows.Text = "Rows:";
            // 
            // numericUpDownColumns
            // 
            this.numericUpDownColumns.Location = new System.Drawing.Point(611, 72);
            this.numericUpDownColumns.Maximum = new decimal(new int[] {
            75,
            0,
            0,
            0});
            this.numericUpDownColumns.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownColumns.Name = "numericUpDownColumns";
            this.numericUpDownColumns.Size = new System.Drawing.Size(48, 20);
            this.numericUpDownColumns.TabIndex = 6;
            this.numericUpDownColumns.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 524);
            this.Controls.Add(this.numericUpDownColumns);
            this.Controls.Add(this.labelRows);
            this.Controls.Add(this.labelColumns);
            this.Controls.Add(this.numericUpDownRows);
            this.Controls.Add(this.buttonGeneratePath);
            this.Controls.Add(this.buttonGenerateMaze);
            this.Controls.Add(this.picutreBox);
            this.Name = "Form1";
            this.Text = "Maze Generator";
            ((System.ComponentModel.ISupportInitialize)(this.picutreBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRows)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColumns)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picutreBox;
        private System.Windows.Forms.Button buttonGenerateMaze;
        private System.Windows.Forms.Button buttonGeneratePath;
        private System.Windows.Forms.NumericUpDown numericUpDownRows;
        private System.Windows.Forms.Label labelColumns;
        private System.Windows.Forms.Label labelRows;
        private System.Windows.Forms.NumericUpDown numericUpDownColumns;
    }
}

