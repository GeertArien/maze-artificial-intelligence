﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Doolhof.LogischeLaag;

/// <author>Geert Ariën</author>
/// <date>24 Mei 2014</date>

namespace Doolhof {
    public partial class Form1 : Form {

        /// <summary>
        /// Het aantal cellen in de x-as
        /// </summary>
        private int cellsX;

        /// <summary>
        /// Het aantal cellen in de y-as
        /// </summary>
        private int cellsY;

        /// <summary>
        /// De afmeting van een cel
        /// </summary>
        private const int CELL_SIZE = 2;

        /// <summary>
        /// Het doolhof object
        /// </summary>
        private Maze maze;

        /// <summary>
        /// Het graphics object dat de weergave beheert
        /// </summary>
        private Graphics display;

        /// <summary>
        /// Standaard constructor, initializeren en instellen componenten
        /// </summary>
        public Form1() {
            InitializeComponent();
            display = picutreBox.CreateGraphics();
            this.buttonGeneratePath.Enabled = false;
        }

        /// <summary>
        /// Rescale and translate the picture box
        /// </summary>
        private void ConfigureDisplay() {
            this.cellsX = (int)this.numericUpDownColumns.Value;
            this.cellsY = (int)this.numericUpDownRows.Value;
            this.maze = new Maze(cellsX, cellsY);
            this.maze.StartCell = this.maze.Cells[cellsX / 2, 0];
            this.maze.EndCell = this.maze.Cells[cellsX / 2, cellsY - 1];

            float schaal = picutreBox.Width / ((cellsX > cellsY ? cellsX : cellsY) * CELL_SIZE + 10f);
            display.ResetTransform();
            display.ScaleTransform(schaal, -schaal); // schaling
            display.TranslateTransform(5f, -(cellsY * CELL_SIZE + 5f));      // oorsprong
        }

        /// <summary>
        /// Het doolhof grafisch weergeven op het scherm
        /// </summary>
        private void DrawMaze() {
            Pen p = new Pen(Color.Black, 0.05f);
            display.DrawLine(p, new Point(0, 0), new Point(0, cellsY * CELL_SIZE));
            display.DrawLine(p, new Point(0, 0), new Point(this.maze.StartCell.PositionX * CELL_SIZE, 0));
            display.DrawLine(p, new Point(this.maze.StartCell.PositionX * CELL_SIZE + CELL_SIZE, 0), new Point(cellsX * CELL_SIZE, 0));

            foreach (Cell cell in this.maze.Cells) {
                if (cell.WallRight == true) {
                    Point point1 = new Point(cell.PositionX * CELL_SIZE + CELL_SIZE, cell.PositionY * CELL_SIZE);
                    Point point2 = new Point(cell.PositionX * CELL_SIZE + CELL_SIZE, cell.PositionY * CELL_SIZE + CELL_SIZE);
                    display.DrawLine(p, point1, point2);
                }
                if (cell.WallTop == true && cell != this.maze.EndCell) {
                    Point point1 = new Point(cell.PositionX * CELL_SIZE, cell.PositionY * CELL_SIZE + CELL_SIZE);
                    Point point2 = new Point(cell.PositionX * CELL_SIZE + CELL_SIZE, cell.PositionY * CELL_SIZE + CELL_SIZE);
                    display.DrawLine(p, point1, point2);
                }
            }
        }

        /// <summary>
        /// De oplossing van het doolhof grafisch weergeven op het scherm
        /// </summary>
        private void DrawPath() {
            Pen p = new Pen(Color.Red, 0.3f);

            Point point1 = new Point(this.maze.EndCell.PositionX * CELL_SIZE + CELL_SIZE / 2, (this.maze.EndCell.PositionY + 1) * CELL_SIZE + CELL_SIZE / 2);
            Point point2 = new Point(this.maze.EndCell.PositionX * CELL_SIZE + CELL_SIZE / 2, this.maze.EndCell.PositionY * CELL_SIZE + CELL_SIZE / 2);
            display.DrawLine(p, point1, point2);

            Cell cell = this.maze.EndCell;

            do {
                point1 = new Point(cell.PositionX * CELL_SIZE + CELL_SIZE / 2, cell.PositionY * CELL_SIZE + CELL_SIZE / 2);
                point2 = new Point(cell.Parent.PositionX * CELL_SIZE + CELL_SIZE / 2, cell.Parent.PositionY * CELL_SIZE + CELL_SIZE / 2);
                display.DrawLine(p, point1, point2);
                cell = cell.Parent;
            } while (cell != this.maze.StartCell);

            point1 = new Point(cell.PositionX * CELL_SIZE + CELL_SIZE /2, cell.PositionY * CELL_SIZE + CELL_SIZE / 2 );
            point2 = new Point(cell.PositionX * CELL_SIZE + CELL_SIZE / 2, (cell.PositionY - 1) * CELL_SIZE + CELL_SIZE / 2);
            display.DrawLine(p, point1, point2);
        }

        /// <summary>
        /// Een nieuw doolhof genereren en weergeven op het scherm
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void buttonGenerateMaze_Click(object sender, EventArgs e) {
            if (this.numericUpDownRows.Value != this.cellsY || this.numericUpDownColumns.Value != this.cellsX) {
                this.ConfigureDisplay();
            }
            this.maze.generateMaze();
            display.Clear(this.picutreBox.BackColor);
            this.DrawMaze();
            this.buttonGeneratePath.Enabled = true;
        }

        /// <summary>
        /// De oplossing van het doolhof genereren en weergeven op het scherm
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void buttonGeneratePath_Click(object sender, EventArgs e) {
            this.maze.generatePath();
            this.DrawPath();
            this.buttonGeneratePath.Enabled = false;
        }
    }
}
